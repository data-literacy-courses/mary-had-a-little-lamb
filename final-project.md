\newpage

# Final Project

This project is **optional** but highly recommended to verify your understanding of
the fundamental programming language constructs. The project is intended to
give you an opportunity to approach computer programming from an algorithmic
or computational perspective. We avoided explicitly introducing computational
thinking because it is rarely appreciated until learners have experienced
writing small programmes a few times. Do not mistake this omission as a
suggestion that it is not an essential skill to be learned by anyone desiring
to solve problems, with or without the aid of a programming language.

The final project will challenge you and take several hours, possibly a few
days before you have a fully functional programme.

Before delving into any code it is time to briefly introduce computational
thinking, also known as algorithmic thinking. Read the scenario in its entirety
and then reread the three paragraphs in which the requirements are given. At
a high level we can identify the following requirements:

- read the literary work
- identify unique words in the literary work
- identify the most common words
- identify the least common words
- produce a report showing the title, author, total number of words, number of
unique words, a list of most common words, and a list of least common words

Next break down each of these high-level requirements into the corresponding
task(s) using simplified English. This procedure should be repeated until the
requirements are transformed into a sequence of steps explaining what, not how,
the programme should convert the input into the desired output. This a called
an algorithm.

- read each line of the literary work
  - if line is empty repeat
- split each line into individual words
  - add number of words in line to total word count
  - increment occurrence count for each word in line
- repeat until no lines remain unread
- count number of unique words
- select words with the single highest frequency
- select words with the single lowest frequency
- output the title, author, total number of words, number of unique words, a
list of most common words, and a list of least common words

Already we can recognise which steps can be implemented directly and which
might benefit from being turned into a user-defined function. While we could
further refine these steps, it is sufficient to translate these steps written
as English words into the programming language used for this course. The
programme skeleton provided for the project should get you started if you
choose to use it as a templated guide with embedded hints and suggestions
concerning how to structure the programming language statements. You will have
to modify the programme skeleton to create a fully functional programme which
runs without errors and produces the correct output.

Implement each step one at a time and run the programme often to observe its
behaviour and especially any errors. Fix the error and continue.

Truthfully, as a beginner, you are not supposed to be able to complete the
final project without a lot of time and effort. We wanted to leave you with a
small programming challenge that forces you to experiment with the language,
review the course materials, especially the *Learner Challenges*, and of course
undertake a bit of research outside the course materials if necessary as you
develop a programmer's mindset.

*If you begin to feel frustrated set it aside, relax, and come back to
it another time.*

## Scenario

The publishing house which owns the rights to "Mary Had a Little Lamb" wants to
determine whether the royalty still being paid to the author's heirs should be
adjusted. The new royalty payment model provides a base amount depending upon
the number of unique words, minus a penalty for each of the most common words
except those in the least common words list.

The report should contain the information illustrated in the *Report Layout*.

You are tasked with producing a report containing the relevant information
about the literary work. The accounts payable clerk will determine the payout
amount from the report.

Your employer runs all applications on a Unix system but does not offer remote
access to anyone other than the operations team. You brought a company-issued
Google ChromeBook to learn how to develop relatively simple programmes in an
effort to eventually automate the reporting.

The report must work for any of the publishing firm's literature handled by the
Children's Acquisition, Life Cycle Management, and Royalty Payment Department.
The report will initially support poetry and short nursery rhymes with books
to be handled at a later unspecified date.

The programme is to be written in AWK. It is recommended you attempt
to include most of the programming language concepts and constructs which
have been presented throughout this course. This is your opportunity to be
creative; there is rarely only one way to implement a programme to produce a
desired outcome/output.

## Report Layout

    Title: Mary Had a Little Lamb
    Author: Sarah Josepha Hale

    Number of Words: 137 (placeholder)
    Number of Unique Words: 52 (placeholder)
    Most Common Word: little (placeholder)
    Least Common Words: fleece snow (placeholder)

*Note: The placeholders represent the type of results your programme should produce
but not these values necessarily.*

## Programme Skeleton

You can use this skeleton to start writing your programme but you are not
required to use it. It simply provides some hints to help you with any
peculiarities of Awk such as: only strings and numbers can be returned by
functions and local variables within a function must be declared in the
function definition header.

The functions `number_of_words()` and `most_common_words()` are two
user-defined functions you might want to create.

```
awk '
  function number_of_words(word_array,   i)
  {
    return 0;
  }
  function most_common_words(
    word_array,   i, occurences, word)
  {
      return "firstword secondword";
  }

  BEGIN { }
  { split("This is a sample line for the programme skeleton", words, " "); }
  END {
    print("");
    print("Title: Mary Had a Little Lamb");
    print("Author: Sarah Josepha Hale");

    print("Number of Words:", number_of_words(words));
    print("Number of Unique Words:", number_of_unique_words);
    print("Most Common Word:", most_common_words(words));
    print("Least Common Words:", least_common_words);
    print("");
  }
' mary_had_a_little_lamb.txt
```
