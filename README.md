# Mary Had a Little Lamb: A First Course in Computer Programming

Acquire the knowledge and skills to create small programmes, focusing on the
absolute minimum programming language concepts shared by every computer
programming language.

**Course Name**: Mary Had a Little Lamb: A First Course in Computer Programming

**Experience Level**: Beginner

**Lessons**: 6

**Time Commitment**: ~8 hours, excluding optional final project

## How to Generate the Course Materials


- Generate course manual in HTML and PDF formats plus emails in plain-text format:
```$ make```

- Generate course manual in HTML and PDF formats:
```$ make manual```

- Generate the course manual as a single page in HTML format:
```$ make manual.html```

- Generate the course manual in PDF format:
```$ make manual.pdf```

- Generate all course lesson and other course content in plain-text format suitable for sending as email:
```$ make email```

- Remove generated files:
```$ make clean```

## License

This work is licensed under the Creative Commons Attribution 4.0 International
License. To view a copy of this license, visit
http://creativecommons.org/licenses/by/4.0/.
