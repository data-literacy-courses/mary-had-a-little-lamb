---
documentclass: scrartcl
title:
- Mary Had a Little Lamb
subtitle:
- A First Course in Computer Programming
author:
- G.D. Horne
papersize:
- letter
fontsize:
- 10pt
geometry:
- margin=1in
fontfamily:
- charter
header-includes:
- \publishers{Dataology Studio 07AD}
- \setlength\parindent{0pt}
- \usepackage{fancyhdr}
- \usepackage{graphicx}
- \pagestyle{fancy}
- \fancyhead[L]{Mary Had a Little Lamb}
- \fancyhead[R]{A First Course in Computer Programming}
- \fancyfoot[L]{2019-05-19}
- \fancyfoot[R]{Version 0.1}
---

\thispagestyle{empty}
\begin{center}{\includegraphics[width=12cm]{mary-had-a-little-lamb-coloring-page-24.jpg}}\end{center}
\clearpage
\pagenumbering{roman}
\newpage
\input{copyright.md}
\newline
\newline
\newline
\input{license-cc-by.md}
\input{repository.md}
\input{third-party-attribution.md}
\newpage
\tableofcontents
\clearpage
\pagenumbering{arabic}
\setcounter{page}{1}
