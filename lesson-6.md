\newpage

# Lesson 6 - Functions

By the end of this lesson you will:

- LO2 *Understand* the purpose of a variable, *the ways in which variables are
  used, and the differences between global and local scope*.
- LO5 *Distinguish between built-in and user-defined functions as well as their
  role in a programme*.
- LO6 *Create a user-defined function and invoke such functions within a
  programme*.

For each learning objective only the *italicised* portion is addressed in the
current lesson.

There are two types of functions available in all programming languages:
built-in functions and user-defined functions. Additionally, some programming
languages provide a facility to access external functions stored in libraries.
This course does not address libraries.

## Built-in Functions

In *Lesson 3* we implemented a programme to read a data file and subsequently
output a message about the number of lines in that file. We have been using the
built-in print() function throughout the course.

Function: `print(string)`

```
awk '
  BEGIN { count = 0; }
  { count++; }
  END {
    if (count == 1) {
      message = "There is" + count + " line";
    } else {
      message = "There are" + count + "lines";
    }
    print(message);
  }
' oneline.txt
```

The wording of the grammatically-correct message was determined by whether the
number of lines was strictly one or any other number including zero. Most
programming languages provide either a built-in function or a library call
enabling modification of a string. Consequently, it is possible to implement
the same functionality as in the preceding programme using a default message,
and a single `if` statement that modifies the default message if and only if
the number of lines is precisely 1.

Function: `sub(substring, replacement_substring, string)`

```
awk '
  BEGIN { count = 0; }
  { count++; }
  END {
    message = "There are" + count + "lines";
    if (count == 1) {
      sub("are", "is", message);
      sub("lines", "line", message);
    }
    print(message);
  }
' oneline.txt
```

Splitting a line of text into individual words and numbers based on a separator
is a common enough task that many programming languages provide either a
built-in function or a library call.

Function: `split(string, array, separator)`

```
awk '
  BEGIN { count = 0; }
  {
    lines[count++] = $0;
  }
  END {
    for (i = 0; i < count; i++) {
      print(lines[i]);
      word_count = split(lines[i], words, " ");
      for (j = 0; j < word_count; j++) {
        print(words[j]);
      }
    }
   }
' mary_had_a_little_lamb.txt
```

Notice the lines which have no words.

Determining the length of a string is possible using either a built-in function
or library call in most programming languages.

Function: `length(string)`

```
awk '
  BEGIN { count = 0; }
  { lines[count++] = $0; }
  END {
    for (i = 0; i < count; i++) {
      print("Line: ", lines[i]);
      n = length(lines[i]);
       print("Line Length:", n, "(characters)");
     }
   }
' mary_had_a_little_lamb.txt
```

Determining the length of an array is possible using either a built-in function
or library call in most programming languages.

Function: `length(array)`

```
awk '
  BEGIN { count = 0; }
  { lines[count++] = $0; }
  END {
    print("File has", count, "lines");
    print("Array has", length(lines), "elements");
  }
' mary_had_a_little_lamb.txt
```

While the `sub()` function replaces the first occurrence of the specified
substring, which can be a single character or multiple contiguous characters,
there are times when we want to replace all occurrences within the target
string. Fortunately, many programming languages provide either a built-in
function or a library function.

Function: `gsub(substring, replacement_string, string)`

```
awk '
  BEGIN { count = 0; }
  { lines[count++] = $0; }
  END {
    for (i = 0; i < count; i++) {
      print(lines[i]);
      m = gsub(/\"|,|;|\.|\?|\s/, "", lines[i]);
       print("Non-Alphabetic Character Count:", m, "\n");
     }
  }
' mary_had_a_little_lamb.txt
```

The first argument to the `gsub()` function is called a regular expression
(regex). For this course all you need know about regexes is the statement
transforms the string so it only contains alphabetic and numeric characters.

Converting the alphabetic characters in a string to lowercase or to uppercase
is available in most programming languages as a built-in function or a library
call.

Function: `tolower(string)`
```
awk ' BEGIN { str = "Fiddlesticks"; print(str, tolower(str)); }
```

Function: `toupper(string)`

```
awk ' BEGIN { str = "Fiddlesticks"; print(str, toupper(str)); }
```

Formatting output is supported by two functions depending whether a formatted
string is dynamically being constructed or displayed on the console.


Function: `printf(format_string, value_1, value_2, ..., value_n)`

```
awk '
  BEGIN { count = 0; }
  {
    count += 1;
  }
  END {
    message = "Total number of lines is";
    printf("%s %d\n", message, count);
  }
' mary_had_a_little_lamb.txt
```

Function: `sprintf(format_string, value_1, value_2, ..., value_n)`

```
awk '
  BEGIN { count = 0; }
  {
    count += 1;
  }
  END {
    message = "Total number of lines is";
    message = sprintf("%s %d", message, count);
    print(message);
  }
' mary_had_a_little_lamb.txt
```

## User-Defined Functions

A programmer can augment the collection of built-in functions included with a
particular programming language by defining user-defined functions. All
user-defined functions must be located before the BEGIN { } clause.

A user-defined function at time of definition takes the form
`function funcation_name(argument_list)`. These functions are called/invoked
the same way as built-in functions.

```
awk '
  function words_in_line(str, sep) {
    word_count = split(str, word_list, sep);
    return word_count;
  }
  BEGIN { }
  {
    line = $0;
    print(line);
    print("Word count:", words_in_line(line, " "));
  }
  END { }
' mary_had_a_little_lamb.txt
```

# Learner Challenge

Associated Learning Outcome(s):

- LO2 *Understand the purpose of a variable, the ways in which variables are
  used, and the differences between global and local scope*.
- LO3 *Be able to alter the execution path of a programme with decision
  statements* and various types of repetition.
- LO5 *Distinguish between built-in and user-defined functions as well as their
  role in a programme*.
- LO6 *Create a user-defined function and invoke such functions within a
  programme*.

1. Modify the programme in LC4-3 to report the number of non-empty lines.
2. Modify the programme in LC6-1 to create a user-define function named
`word_count` which takes a string as an argument and returns the number of
words in the string. For each non-empty line in the nursery rhyme, display the
content of the line followed by the number of words in the line surrounded by
parentheses ().
