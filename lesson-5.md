\newpage

# Lesson 5 - Variable Scope

By the end of this lesson you will:

- LO2 *Understand* the purpose of a variable, *the ways in which variables are
  used, and the differences between global and local scope*.

For each learning objective only the *italicised* portion is addressed in the
current lesson.

## Global Scope versus Local Scope

Variables defined within a programme have global scope; the variable can be
read and modified anywhere in the programme including in user-defined functions
unless the user-defined function declares a variable of the same name. Variables
declared within a user-defined function have global scope by default unless the
variable is declared in the function definition header as an argument. The
following programmes show how variables are affected by scoping rules.

```
awk '
  function words_in_line(str) {
    word_count = split(str, word_list, " ");
    return word_count;
  }
  BEGIN { }
  {
    line = $0;
    print(line);
    print("Word count:", words_in_line(line));
    print("Word count:", word_count, "\n");
  }
  END { }
' mary_had_a_little_lamb.txt
```

To avoid scope leaks any variables defined within a function must be specified
as an argument in the function definition. By convention these local variables
are offset from the arguments in the definition specification and not passed to
the function when invoked. The offset of local variables is a convention used
by programmers for Awk programmes, not a requirement of the Awk language
specification nor the Awk interpreter. Making local variables part of the
function header is an Awkism. Other programming languages take a less awkward
approach to limiting the scope of a local variable to the function.

```
awk '
  function words_in_line(str,    word_count) {
    word_count = split(str, word_list, " ");
    return word_count;
  }
  BEGIN { }
  {
    line = $0;
    print(line);
    print("Word count:", words_in_line(line));
    print("Word count:", word_count, "\n");
  }
  END { }
' mary_had_a_little_lamb.txt
```

Notice this time the variable *word_count* in the statement
`print("Word count:", word_count, "\n");`
has a special value. In the case of the programming language chosen for this
course, Awk, a variable defaults to null value (ASCII NUL); it is treated as
empty string or zero depending whether the variable has any mathematical
operation applied to it..