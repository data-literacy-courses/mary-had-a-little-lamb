\newpage

Welcome to the fifth lesson in the
*Mary Had a Little Lamb: A First Course in Computer Programming*
course as you progress towards your goal of learning how to programme.
Today's lesson introduces the fifth core language feature: (v) variable
scoping.

