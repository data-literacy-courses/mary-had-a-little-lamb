\newpage

# Lesson 1 - Structure of a Programme Executable by AWK

By the end of this lesson you will:

- LO1 *Be able to describe the structure of a programme implemented with AWK.*

For each learning objective only the *italicised* portion is addressed in the
current lesson.

## Generalised Model of an AWK Programme

```
awk '
  BEGIN { }
  { }
  END { }
' data_file_name
```

Ordinarily, the less compact form is preferred when specifying a programme and
it is this form we will use throughout the course materials. As with all rules
there is an exception: some examples may use the compact form to save space on
the page.

```
awk 'BEGIN { action } generic { action } END { action }' data_file_name
```

\begin{figure}\begin{center}{\includegraphics[width=5cm]{awk-workflow.jpg}}\end{center}\end{figure}

The BEGIN { } and END { } clauses can only appear at most once in a programme.
In contrast, a programme may contain zero or more generic { } clauses. A
generic clause may be preceded by an optional condition, if the actions within
the clause should only be performed when a line of text matches the condition.
This course does not introduce the conditional form of the generic clause.

Similarly, BEGIN { }, END { }, and generic {} clauses are presented as a means
to help us separate the code that is to be run before any lines are read, as
each line is read, and after all lines have been read by our programme,
respectively.

**The read-evaluate-print-loop (REPL) model integrated into AWK is especially
beneficial to beginning programmers.**

Programme listings emphasise readability, avoiding "Awkisms."
Statement-specific syntax have their equivalents in other programming
languages.

## The Simplest AWK Programme

Before the first programming concept, variables, is introduced let's quickly
review a familiar pattern. It is used as a template for every programme and
serves as a visual cue.

```
awk '
  BEGIN { }
  { print($0); }
  END { }
' mary_had_a_little_lamb.txt
```


What is the purpose of this simple programme? If you replied. "The programme
reads the file named `mary_had_a_little_lamb.txt` one line at a time and outputs
each line to the console," you are ready to proceed to the next lesson.

When using the Interactive Programming Environment at `http://awk.js.org`
the contents of `mary_had_a_little_lamb.txt` should be pasted into the STDIN
panel and the programme between the single-quotes should be pasted into the
Command Line panel. The output, if any, will appear in the STDOUT/STDERR panel.

# Learner Challenge

Associated Learning Outcome(s):

- LO1 *Be able to describe the structure of a programme implemented with AWK.*

1. Modify the programme so the print() statement is only in the BEGIN {} clause
and compare the output with the prior version in which the print() statement
was in the generic clause { print($0); }.
2. Modify the programme so the print() statement is only in the END {} clause
and compare the output with the prior version in which the print() statement
was in the generic clause { print($0); }.
3. What accounts for these differences in behaviour?
