\newpage

Today marks the end of your first small step on your journey toward mastering
computer programming. A set of model solutions for each of the learner
challenges can be found further down in this email. Additionally, an optional
final project awaits you to consolidate and solidify your comprehension of the
fundamentals of computer programming.
