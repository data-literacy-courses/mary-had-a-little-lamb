\newpage

# Mary Had a Little Lamb: A First Course in Computer Programming

Welcome to *Mary Had a Little Lamb: A First Course in Computer Programming*.
You will acquire the skills to create small programmes, focusing on the
absolute minimum programming language concepts shared, in one form or another,
by every programming language: (i) programme structure, (ii) variables,
(iii) decisions and branching, (iv) loops and repetition, (v) variable scope,
and (vi) functions.

If you commit to devoting approximately 8 hours in total, excluding the final
project, you can learn the basics of computer programming.

Why use a nursery rhyme rather than the typical numerical approach to introduce
the fundamentals of computer programming?

# Who is the Audience for the Course?

Reading, writing, and arithmetic became increasingly important skills for the
working class at the dawn of the Industrial Revolution. Today, in the midst of
the Information Revolution an additional literacy skill has emerged - computer
programming. As reading and writing are to general literacy, computer
programming is to data literacy.

Before creating *Mary Had a Little Lamb: A First Course in Computer Programming*,
a beginner-friendly programming course, a conscious decision was made to
introduce the core skills required to write computer programmes for purposes
other than application development or web development. You might want to learn
programming as a hobby, as a way to automate tasks in the workplace, or to
understand the world around you through data analytics or data science and
machine learning.

# Final Project Preview

The publishing house, which recently hired you, owns the rights to "Mary Had a
Little Lamb" and wants to determine whether the royalty still being paid to the
author's heirs should be adjusted. The original English version of
"Mary Had a Little Lamb," a nursery rhyme written by Sarah Josepha Hale, serves
as the backdrop to our adventure. The full text can be retrieved from the
Poetry Foundation website
**https://www.poetryfoundation.org/poems/46954/mary-had-a-little-lamb**.

The new royalty payment model proposed by the publishing house provides a base
amount depending upon the number of unique words, minus a penalty for each of
the most common words except those in the least common words list. You are
tasked with producing a report containing the relevant information about the
literary work. The accounts payable clerk will determine the payout amount.

By the time you complete the 6 lessons you will be able to create a programme
to produce the report shown on the next page.

    Title: Mary Had a Little Lamb
    Author: Sarah Josepha Hale

    Number of Words: 137 (placeholder)
    Number of Unique Words: 52 (placeholder)
    Most Common Word: little (placeholder)
    Least Common Words: fleece snow (placeholder)

Instructions about the final project will be provided at the end of the
six instructional lessons.

# Interactive Programming Environment Walk-through

All programming activities can be performed using only the web browser already
available on your computer, tablet, or even smartphone. However, from a user
ergonomics perspective a computer or tablet with physical keyboard is
recommended.

In the web browser, navigate to
  **http://awk.js.org**
to load the interactive programming environment that we will use to write each
of the demonstration programmes, and you will use for the hands-on practical
exercises at the end of each lesson.

```{awk}
awk 'BEGIN { } { print($0); } END { }' mary_had_a_little_lamb.txt
```

In the Command Line panel, copy-and-paste the content between the
single-quotes in the line above. In the STDIN panel copy-and-paste the entirety
of the nursery rhyme including empty lines. Observe the results in the
STDOUT|STDERR panel.

```
+-----------------------------------------------------------------------------+
| Command Line: 'BEGIN { } { print($0); } END { }'                            |
+-----------------------------------------------------------------------------+
| STDIN                                | STDOUT/STDERR                        |
+--------------------------------------+--------------------------------------+
|Copy and paste the nursery rhyme into |Copy and paste the nursery rhyme into |
|the STDIN panel and see the results of|the STDIN panel and see the results of|
|of the programme shown in the Command |of the programme shown in the Command |
|Line panel.                           |Line panel.                           |
|                                      |                                      |
|The Command Line panel is only one    |The Command Line panel is only one    |
|line high. When you copy-and-paste the|line high. When you copy-and-paste the|
|programmes from the course lessons    |programmes from the course lessons    |
|you can use the cursor keys to view   |you can use the cursor keys to view   |
|each line. You can edit your programme|each line. You can edit your programme|
|in the Command Line panel but it might|in the Command Line panel but it might|
|be easier to make any changes locally |be easier to make any changes locally |
|and copy-and-paste the updated        |and copy-and-paste the updated        |
|programme into the Command Line panel.|programme into the Command Line panel.|
+-----------------------------------------------------------------------------+
```
*Note:* Once you close the web browser tab your content is deleted from the
interactive programming environment. To preserve your programmes use a text
editor on your computer to write the programme code, save it locally, and
copy the programme code to the Command Line panel.

In the instructions when you see a file name, instead of typing the file name
as part of the programme, paste the content of the nursery rhyme
(mary_had_a_little_lamb.txt) or type a single line of text (oneline.txt) into
the STDIN panel, and delete the current contents of STDIN whenever you want an
empty file (empty.txt).

# The Programmer's (Hacker) Mindset

The programmer's mindset is one of curiosity, experimentation, problem solving,
research and knowing when and how to ask for help. To err is human; programmers
are human which means we can expect to regularly write imperfect programmes.
Hence, another dimension of the programmer's mindset is nurturing debugging
skills and patience.

# Why AWK as the Teaching Language?

The course focuses on core fundamental programming priniciples. AWK is the
language chosen to teach these principles because of its support for the
read-evaluate-print-loop (REPL) model, especially beneficial to beginning
programmers.

A plethora of programming languages are available, some better suited than
others for the beginning programmer. Are you wondering why we did not select
Python, another REPL language although only when in interactive mode? Python
is a fine general-purpose programming language and is taught in many first-year
computer science curriculums. It brings a level of cognitive overload
ill-suited to the needs of non-software engineers as a first programming
language.

On the other hand, AWK is a data-centric programming language with only
minimalist expectations of the user. AWK was designed as a programming language
for non-programmers.

Earlier we saw an example of AWK in action as part of the Interactive
Programming Environment Walk-through. You probably knew what the programme was
going to do before seeing it run.

The equivalent programme in Python is an example of how AWK simplifies even
this trivial task.

```{python}
fh = open('mary_had_a_little_lamb.txt', 'r')
line = fh.readline()
while line:
    print(line)
    line = fh.readline()
fh.close()
```
In Python whitespace (indentation) and strictly enforced layout of the
statements are part of the semantics of the language; a source of confusion
when an error occurs unexpectedly.

In contrast, AWK ignores whitespace and formatting within the paired
single-quotes with the exception of strings enclosed in double-quotes.

```
awk 'BEGIN { FS = " "; } { print($0); } END { }' mary_had_a_little_lamb.txt
awk 'BEGIN { FS = " " } { print($0) } END { }' mary_had_a_little_lamb.txt
awk 'BEGIN {FS = " "} {print($0)} END {}' mary_had_a_little_lamb.txt
awk 'BEGIN{FS = " "}{print($0)}END{}' mary_had_a_little_lamb.txt
```
For readability it is recommended that you use the style presented in the
upcoming lessons. The semicolon is only required when there are multiple
statements in a clause { }. However, in the code examples we end all statements
with a semicolon.

# Learning Objectives

As you progress through each lesson you will gain insight into programming
constructs associated with one or more of the following learning objectives.

- LO1 Be able to describe the structure of a programme implemented with AWK.
- LO2 Understand the purpose of a variable, the ways in which variables are
  used, and the differences between global and local scope.
- LO3 Be able to alter the execution path of a programme with decision
  statements and various types of repetition.
- LO4 Use a basic data structure called an array to store a collection of
  related elements (items) instead of a separate variable for each.
- LO5 Distinguish between built-in and user-defined functions as well as their
  role in a programme.
- LO6 Create a user-defined function and invoke such functions within a
  programme.

While waiting for Lesson 1 to begin, read about the genesis of the AWK
programming language.

*https://www.computerworld.com.au/article/216844/a-z_programming_languages_awk/*
