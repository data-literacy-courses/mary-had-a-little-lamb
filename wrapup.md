\newpage

# Congratulations!

Thank you for participating in the
*Mary Had a Little Lamb: A First Course in Computer Programming* course.

"A journey of a thousand miles (kilometres) begins with a single step,"
attributed to Laozi, an ancient Chinese philosopher and writer.

Are you curious and eager to continue the adventure in
creative problem solving using your newly acquired programming knowledge and
skills?

As a course participant your feedback is very important. A short survey will be
emailed within a few days of completing the course; consider taking a few
minutes to evaluate the effectiveness of this course.
