course: manual email


email: email-0.txt email-1.txt email-2.txt email-3.txt email-4.txt email-5.txt email-6.txt email-7.txt email-8.txt

email-0.txt: email-0.md registration-signup-confirmation.md
	docker run --rm -it --volume ${PWD}:/opt pandoc:2.3.1 \
		--from=markdown --to=plain \
		--output=/opt/email-0.txt \
		/opt/email-0.md \
		/opt/registration-signup-confirmation.md

email-1.txt: email-1.md welcome.md license-cc-by.plain-text.md
	docker run --rm -it --volume ${PWD}:/opt pandoc:2.3.1 \
		--from=markdown --to=plain \
		--output=/opt/email-1.txt \
		/opt/email-1.md \
		/opt/welcome.md \
		/opt/license-cc-by.plain-text.md

email-2.txt: email-2.md lesson-1.md license-cc-by.plain-text.md
	docker run --rm -it --volume ${PWD}:/opt pandoc:2.3.1 \
    --from=markdown --to=plain \
    --output=/opt/email-2.txt \
    /opt/email-2.md \
    /opt/lesson-1.md \
    /opt/license-cc-by.plain-text.md

email-3.txt: email-3.md lesson-2.md license-cc-by.plain-text.md
	docker run --rm -it --volume ${PWD}:/opt pandoc:2.3.1 \
    --from=markdown --to=plain \
    --output=/opt/email-3.txt \
    /opt/email-3.md \
    /opt/lesson-2.md \
    /opt/license-cc-by.plain-text.md

email-4.txt: email-4.md lesson-3.md license-cc-by.plain-text.md
	docker run --rm -it --volume ${PWD}:/opt pandoc:2.3.1 \
    --from=markdown --to=plain \
    --output=/opt/email-4.txt \
    /opt/email-4.md \
    /opt/lesson-3.md \
    /opt/license-cc-by.plain-text.md

email-5.txt: email-5.md lesson-4.md license-cc-by.plain-text.md
	docker run --rm -it --volume ${PWD}:/opt pandoc:2.3.1 \
		--from=markdown --to=plain \
		--output=/opt/email-5.txt \
		/opt/email-5.md \
		/opt/lesson-4.md \
		/opt/license-cc-by.plain-text.md

email-6.txt: email-6.md lesson-5.md license-cc-by.plain-text.md
	docker run --rm -it --volume ${PWD}:/opt pandoc:2.3.1 \
		--from=markdown --to=plain \
		--output=/opt/email-6.txt \
		/opt/email-6.md \
		/opt/lesson-5.md \
		/opt/license-cc-by.plain-text.md

email-7.txt: email-7.md lesson-6.md license-cc-by.plain-text.md
	docker run --rm -it --volume ${PWD}:/opt pandoc:2.3.1 \
		--from=markdown --to=plain \
		--output=/opt/email-7.txt \
		/opt/email-7.md \
		/opt/lesson-6.md \
		/opt/license-cc-by.plain-text.md

email-8.txt: email-8.md wrapup.md learner-challenge-model-solutions.md final-project.md license-cc-by.plain-text.md
	docker run --rm -it --volume ${PWD}:/opt pandoc:2.3.1 \
		--from=markdown --to=plain \
		--output=/opt/email-8.txt \
		/opt/email-8.md \
		/opt/wrapup.md \
		/opt/learner-challenge-model-solutions.md \
		/opt/final-project.md \
		/opt/license-cc-by.plain-text.md


manual: manual.html manual.pdf

manual.html: copyright.md license-cc-by.html.md third-party-attribution.md welcome.md lesson-1.md lesson-2.md lesson-3.md lesson-4.md lesson-5.md lesson-6.md wrapup.md final-project.md learner-challenge-model-solutions.md
	docker run --rm -it --volume ${PWD}:/opt pandoc:2.3.1 \
    --from=markdown --to=html \
    --output=/opt/manual.html \
    /opt/welcome.md \
    /opt/lesson-1.md \
		/opt/lesson-2.md \
    /opt/lesson-3.md \
		/opt/lesson-4.md \
    /opt/lesson-5.md \
		/opt/lesson-6.md \
    /opt/wrapup.md \
    /opt/final-project.md \
		/opt/license-cc-by.html.md \
		/opt/copyright.md

manual.pdf: frontmatter.md copyright.md license-cc-by.md third-party-attribution.md repository.md welcome.md lesson-1.md lesson-2.md lesson-3.md lesson-4.md lesson-5.md lesson-6.md wrapup.md final-project.md learner-challenge-model-solutions.md
	docker run --rm -it --volume ${PWD}:/opt pandoc:2.3.1 \
		--from=markdown --to=latex \
		--output=/opt/manual.pdf \
		/opt/frontmatter.md \
		/opt/welcome.md \
		/opt/lesson-1.md \
		/opt/lesson-2.md \
		/opt/lesson-3.md \
		/opt/lesson-4.md \
		/opt/lesson-5.md \
		/opt/lesson-6.md \
		/opt/wrapup.md \
		/opt/final-project.md \
		/opt/learner-challenge-model-solutions.md


website: welcome.html lesson-1.html lesson-2.html lesson-3.html lesson-4.html lesson-5.html lesson-6.html project.html

welcome.html: welcome.md license-cc-by.html.md
	docker run --rm -it --volume ${PWD}:/opt pandoc:2.3.1 \
		--from=markdown --to=html \
		--output=/opt/welcome.html \
		/opt/welcome.md \
		/opt/license-cc-by.html.md

lesson-1.html: lesson-1.md license-cc-by.html.md
	docker run --rm -it --volume ${PWD}:/opt pandoc:2.3.1 \
		--from=markdown --to=html \
		--output=/opt/lesson-1.html \
		/opt/lesson-1.md \
		/opt/license-cc-by.html.md

lesson-2.html: lesson-2.md license-cc-by.html.md
	docker run --rm -it --volume ${PWD}:/opt pandoc:2.3.1 \
		--from=markdown --to=html \
		--output=/opt/lesson-2.html \
		/opt/lesson-2.md \
		/opt/license-cc-by.html.md

lesson-3.html: lesson-3.md license-cc-by.html.md
	docker run --rm -it --volume ${PWD}:/opt pandoc:2.3.1 \
		--from=markdown --to=html \
		--output=/opt/lesson-3.html \
		/opt/lesson-3.md \
		/opt/license-cc-by.html.md

lesson-4.html: lesson-4.md license-cc-by.html.md
	docker run --rm -it --volume ${PWD}:/opt pandoc:2.3.1 \
		--from=markdown --to=html \
		--output=/opt/lesson-4.html \
		/opt/lesson-4.md \
		/opt/license-cc-by.html.md

lesson-5.html: lesson-5.md license-cc-by.html.md
	docker run --rm -it --volume ${PWD}:/opt pandoc:2.3.1 \
		--from=markdown --to=html \
		--output=/opt/lesson-5.html \
		/opt/lesson-5.md \
		/opt/license-cc-by.html.md

lesson-6.html: lesson-6.md license-cc-by.html.md
	docker run --rm -it --volume ${PWD}:/opt pandoc:2.3.1 \
		--from=markdown --to=html \
		--output=/opt/lesson-6.html \
		/opt/lesson-6.md \
		/opt/license-cc-by.html.md

project.html: final-project.md license-cc-by.html.md
	docker run --rm -it --volume ${PWD}:/opt pandoc:2.3.1 \
		--from=markdown --to=html \
		--output=/opt/project.html \
		/opt/final-project.md \
		/opt/license-cc-by.html.md


clean:
	rm -f *.html *.pdf *.txt
