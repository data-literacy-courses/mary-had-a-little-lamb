The repository for the source materials can be retrieved from
https://gitlab.com/gregorydhorne/courses/mary-had-a-little-lamb.

The container image referenced in the Makefile can be retrieved from
https://hub.docker.com/r/gdhorne/pandoc-in-a-box. The source repository can be
retrieved from https://gitlab.com/gregorydhorne/pandoc-in-a-box and licensed
under the Berkeley Source Distribution (BSD) 3-Clause BSD License. To view this
license, visit https://gitlab.com/gregorydhorne/pandoc-in-a-box/LICENSE-BSD.
