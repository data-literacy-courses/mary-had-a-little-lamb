\newpage

# Lesson 3 - Decisions and Branching

By the end of this lesson you will:

- LO3 *Be able to alter the execution path of a programme with decision
  statements* and various types of repetition.

For each learning objective only the *italicised* portion is addressed in the
current lesson.

Decision statements and looping statements add a level of
control over the order in which programming language statements are performed.

Previously, the sample programmes executed in the order the statements appeared
within each of the three clause types. That is to say, the statements in the
BEGIN { } clause were performed in sequential order without deviation; then the
statements in the generic { } clause were performed in sequential order; and
finally the statements in the END { } clause were performed in sequential
order.

Let's reexamine the line counting programmes from *Lesson 2*. After rewriting,
the updated programme is ready to demonstrate a situation in which we can
adjust the descriptive output depending on the number of lines in the data file.

The revised programme reading a multi-line file,

```
awk '
  BEGIN { count = 0; }
  { count++; }
  END { print("There are", count, "lines"); }
' mary_had_a_little_lamb.txt
```

Next the revised programme reading a file containing no lines.

```
awk '
  BEGIN { count = 0; }
  { count++; }
  END { print("There are", count, "lines"); }
' empty.txt
```

Finally the revised programme reading a file containing only one line.

```
awk '
  BEGIN { count = 0; }
  { count++; }
  END { print("There are", count, "lines"); }
' oneline.txt
```

What do you notice about the message displayed by the programme when the data
file contains zero lines, one line, and more than one line?

The programme can produce a grammatically correct message by using a decision
statement. But should we use the IF or IF-ELSE form?

\newpage

## IF Statement

```
awk '
  BEGIN { count = 0; }
  { count++; }
  END {
    if (count == 1) {
      message = "There is" + count + "line";
    }
    if (count != 1) {
      message = "There are" + count + "lines";
    }
    print(message);
  }
' oneline.txt
```

## IF-ELSE Statement

```
awk '
  BEGIN { count = 0; }
  { count++; }
  END {
    if (count == 1) {
      message = "There is" + count + "line";
    } else {
      message = "There are" + count + "lines";
    }
    print(message);
  }
' oneline.txt
```

When trying to decide between these decision statements it is helpful to pause
and ask ourselves whether we are handling an exception or a single condition
with two possible outcomes.

In an upcoming lesson we will learn another way to customise the message using
a built-in function, a default value, and a single IF statement.

# Learner Challenge

Associated Learning Outcome(s):

- LO1 *Be able to describe the structure of a programme implemented with AWK.*
- LO2 *Understand the purpose of a variable, the ways in which variables are
  used*, and the differences between global and local scope.
- LO3 *Be able to alter the execution path of a programme with decision
  statements* and various types of repetition.

1. Modify the evens and odds programme from *Lesson 2 - Variables* to use a
decision statement to determine whether each nursery rhyme line is even or odd
and then update the appropriate variable.
2. With your knowledge about variables and decision statements is there a way
to reduce the number of times the "even-odd" modulo computation is executed?
3. Modify the programme in LC3-2 to only count non-empty lines.