\newpage

Welcome to the first lesson in the
*Mary Had a Little Lamb: A First Course in Computer Programming*
course as you progress towards your goal of learning how to programme.
Today's lesson introduces the first core language feature: (i) programme
structure.
