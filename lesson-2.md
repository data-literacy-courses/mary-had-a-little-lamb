\newpage

# Lesson 2 - Variables

By the end of this lesson you will:

- LO2 *Understand the purpose of a variable and the ways in which variables
  are used*, and the differences between global and local scope.

For each learning objective only the *italicised* portion is addressed in the
current lesson.

## Variable

A variable takes the form **variable_name = value**, where variable_name must
begin with an alphabetic character.

Count the number of lines in the nursery rhyme

```
awk '
  BEGIN { count = 0; }
  { count = count + 1; print(count, $0); }
  END { print(count); }
' mary_had_a_little_lamb.txt
```

A shorthand notation available in many programming languages for incrementing
(increasing) or decrementing (decreasing) a value from a variable and assigning
the result to the same variable is shown below.

The statement `count = count + 1` can be abbreviated as `count += 1`.

```
awk '
  BEGIN { count = 0; }
  { count += 1; print(count, $0); }
  END { print(count); }
' mary_had_a_little_lamb.txt
```

The same syntax can be used for subtraction (-=), multiplication (*=), division
(/=), and modulo (%=). The numeric value on the right-side of the assignment
operator is **not** restricted to 1 and can even be a fractional value.

An even more concise form of the statement, if the constant incremental value
is 1, takes the form

```
awk '
  BEGIN { count = 0; }
  {
    count++;
    print(count, $0);
  }
  END { print("Total number of lines is", count); }
' mary_had_a_little_lamb.txt
```

All programming languages support the form `count = count + 1` while many
others support the forms `count += 1` and/or `count++` as well.

Now that we know how to count the number of lines in the nursery rhyme we can
calculate the quantity of even-numbered lines and odd-numbered lines.

\newpage

```
awk '
  BEGIN {
    total_line_count = 0;
    even_numbered_line_count = 0;
    odd_numbered_lined_count = 0;
  }
  {
    total_line_count = total_line_count + 1;
    even_numbered_line_count +=  1 - (total_line_count % 2);
    odd_numbered_line_count += (total_line_count % 2);
  }
  END {
    print("Total number of lines is", total_line_count);
    print("Number of even numbered lines is", even_numbered_line_count);
    print("Number of odd numbered lines is", odd_numbered_line_count);
  }
' mary_had_a_little_lamb.txt
```

The mathematical operators simplified these calculations a bit.

# Learner Challenge

Associated Learning Outcome(s):

- LO1 *Be able to describe the structure of a programme implemented with AWK.*
- LO2 *Understand the purpose of a variable and the ways in which variables are
  used*, and the differences between global and local scope.

1. Which of the various forms of the addition (increment) operator supports
any value for the constant? This constant can be a variable too.
2. Modify the even and odd line counter to reduce the number of calculations
necessary to determine if the current total line count is even or odd.
3. Write a programme which initialises a variable with the value 100,
assign the remainder after dividing by a odd number to another variable before
incrementing the remainder by one more than the value of the first variable
and reassign the new value to the second variable. Print the value of both
variables and label the output.
