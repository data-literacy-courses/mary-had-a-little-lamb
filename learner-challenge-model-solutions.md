\newpage

# Learner Challenge Model Solutions

Learner's solutions are not required to exactly match these exemplar
programming model solutions. The provided solutions might be more verbose in
the interest of clarity and for educational purposes. There is rarely only one
way to implement a programme to produce a desired outcome/output, as long as
the instructions in each challenge are followed.

---

## Lesson 1 - Structure of a Programme Executable by AWK

- LC1-1: *Modify the programme so the print() statement is only in the BEGIN {}
clause and compare the output with the prior version in which the print()
statement was in the generic clause { print($0); }*.

```
     awk '
       BEGIN { print($0); }
       { }
       END { }
     ' mary_had_a_little_lamb.txt
```

- LC1-2: *Modify the programme so the print() statement is only in the END {}
clause and compare the output with the prior version in which the print()
statement was in the generic clause { print($0); }*.

```
     awk '
     BEGIN { }
     { }
     END { print($0); }
     ' mary_had_a_little_lamb.txt
```

- LC1-3: *What accounts for these differences in behaviour*?

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
The BEGIN { print($0); } clause executes before any lines in the file named
*mary_had_a_little_lamb.txt* is read. Therefore, the *print()* statement
outputs the value currently assigned to built-in variable $0 which by default
is an empty string.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
The generic { print($0); } clause executes as each line in the file named
*mary_had_a_little_lamb.txt* is read. The built-in variable $0 is assigned a
value equal to the content of the current line in the file beginning with the
first line and finishing with the last line in the file.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
The END { print($0); } clause executes after all lines in the file named
*mary_had_a_little_lamb.txt* are read. Therefore, the *print()* statement
outputs the value currently assigned to built-in variable $0 which is the last
line of the file.


## Lesson 2 - Variables

- L2-1: *Which of the various forms of the addition (increment) operator
supports any value for the constant? This constant can be a variable too*.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
The increment operator forms `variable = variable + value` and
`variable += value` support any value for the increment value.

- LC2-2: *Modify the even and odd line counter to reduce the number of
calculations necessary to determine if the current total line count is even or
odd*.

```
     awk '
       BEGIN {
         total_line_count = 0;
         even_numbered_line_count = 0;
         odd_numbered_lined_count = 0;
       }
       {
         total_line_count = total_line_count + 1;
         remainder = total_line_count % 2;
         even_numbered_line_count +=  1 - remainder;
         odd_numbered_line_count += remainder;
       }
       END {
         print("Total number of lines is", total_line_count);
         print("Number of even numbered lines is", even_numbered_line_count);
         print("Number of odd numbered lines is", odd_numbered_line_count);
       }
     ' mary_had_a_little_lamb.txt
```

- LC2-3: *Write a programme which initialises a variable with the value 100,
assign the remainder after dividing by a odd number to another variable before
incrementing the remainder by one more than the value of the first variable
and reassign the new value to the second variable. Print the value of both
variables and label the output*.

```
     awk '
       BEGIN {
         var1 = 100;
         var2 = var1 % 3;
         var2 = var2 + (var1 + 1);}
       { }
       END {
         print("var1 =", var1);
         print("var2 =", var2);
       }
     '
```

## Lesson 3 - Decisions and Branching

- LC3-1: *Modify the evens and odds programme from* *Lesson 2 - Variables* *to
use a decision statement to determine whether each nursery rhyme line is even
or odd and then update the appropriate variable*.

```
     awk '
       BEGIN {
         total_line_count = 0;
         even_numbered_line_count = 0;
         odd_numbered_lined_count = 0;
       }
       {
         total_line_count = total_line_count + 1;
         if (total_line_count % 2 == 0) {
           even_numbered_line_count +=  1 - (total_line_count % 2);
         } else {
           odd_numbered_line_count += (total_line_count % 2);
         }
       }
       END {
         print("Total number of lines is", total_line_count);
         print("Number of even numbered lines is", even_numbered_line_count);
         print("Number of odd numbered lines is", odd_numbered_line_count);
       }
     ' mary_had_a_little_lamb.txt
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
If you read the instructions carefully, the programme to be modified is from
*Lesson 2*, not LC2-2.

- LC3-2: *With your knowledge about variables and decision statements is there
a way to reduce the number of times the "even-odd" modulo computation is
executed*?

```
     awk '
       BEGIN {
         total_line_count = 0;
         even_numbered_line_count = 0;
         odd_numbered_lined_count = 0;
       }
       {
         total_line_count = total_line_count + 1;
         remainder = total_line_count % 2;
         if (remainder == 0) {
           even_numbered_line_count +=  1 - remainder;
         } else {
           odd_numbered_line_count += remainder;
         }
       }
       END {
         print("Total number of lines is", total_line_count);
         print("Number of even numbered lines is", even_numbered_line_count);
         print("Number of odd numbered lines is", odd_numbered_line_count);
       }
     ' mary_had_a_little_lamb.txt
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
By combining the solution from L2-2 with LC3-1 the fewest number of times the
"even-odd" modulo computation is executed can be implemented.

- LC3-3: *Modify the programme in LC3-2 to only count non-empty lines*.

```
     awk '
       BEGIN {
         total_line_count = 0;
         even_numbered_line_count = 0;
         odd_numbered_lined_count = 0;
       }
       {
         if ($0 != "") {
           total_line_count = total_line_count + 1;
           remainder = total_line_count % 2;
           if (remainder == 0) {
             even_numbered_line_count +=  1 - remainder;
           } else {
             odd_numbered_line_count += remainder;
           }
         }
       }
       END {
         print("Total number of lines is", total_line_count);
         print("Number of even numbered lines is", even_numbered_line_count);
         print("Number of odd numbered lines is", odd_numbered_line_count);
       }
     ' mary_had_a_little_lamb.txt
```
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Can you spot any other ways to reduce the number of programming language
statements in this solution and yet obtain the same output?

## Lesson 4 - Arrays, Loops and Repetition

- LC4-1: *Modify the file reading programmes so that the statement block within
the loop does not execute at all, if possible*.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
**FOR Loop**

```
     awk '
       BEGIN { count = 0; }
       { lines[count++] = $0; }
       END {
         for (i = count; i < count; i++) {
           print(lines[i]);
         }
       }
     ' mary_had_a_little_lamb.txt
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
**WHILE Loop**

```
     awk '
       BEGIN { count = 0; }
       { lines[count++] = $0; }
       END {
         i = count;
         while (i < count) {
           print(lines[i]);
           i++;
         }
       }
     ' mary_had_a_little_lamb.txt
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
**DO-WHILE Loop**

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
The statement block enclosed within a DO-WHILE loop will always executed at
least one time because the termination condition is not evaluated until the
loop has already been executed.

- LC4-2: *Modify each version of the file reading programmes demonstrated in
this lesson by reversing the order in which the lines of the nursery rhyme are
displayed, that is, print the nursery rhyme beginning with the last line and
ending with the first line*.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
**FOR Loop**

```
     awk '
       BEGIN { count = 0; }
       { lines[count++] = $0; }
       END {
         for (i = count - 1; i >= 0; i--) {
           print(lines[i]);
         }
       }
     ' mary_had_a_little_lamb.txt
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
**WHILE Loop**

```
     awk '
       BEGIN { count = 0; }
       { lines[count++] = $0; }
       END {
         i = count - 1;
         while (i >= 0) {
           print(lines[i]);
           i--;
         }
       }
     ' mary_had_a_little_lamb.txt
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
**DO-WHILE Loop**

```
     awk '
       BEGIN { count = 0; }
       { lines[count++] = $0; }
       END {
         i = count - 1;
         do {
           print(lines[i]);
           --i;
         } while (i >= 0)
       }
     ' mary_had_a_little_lamb.txt
```

- LC4-3: *Modify any version of the file reading programme demonstrated in this
lesson such that only non-empty lines are stored in the array and display the
resulting array contents*.

```
     awk '
       BEGIN { count = 0; }
       {
         if ($0 != "") {
           lines[count++] = $0;
         }
       }
       END {
         for (i = 0; i < count; i++) {
           print(lines[i]);
         }
       }
     ' mary_had_a_little_lamb.txt
```

## Lesson 5 - Variable Scope

None.



## Lesson 6 - Functions

- LC6-1: *Modify the programme in LC4-3 to report the number of non-empty
lines*.

```
     awk '
       BEGIN { count = 0; }
       {
         if ($0 != "") {
           lines[count++] = $0;
         }
       }
       END { print("Total non-empty lines:", count); }
     ' mary_had_a_little_lamb.txt
```

- LC6-2: *Modify the programme in LC6-1 to create a user-define function named*
`word_count` *which takes a string as an argument and returns the number of
words in the string. For each non-empty line in the nursery rhyme, display the
content of the line followed by the number of words in the line surrounded by
parentheses ().*

```
     awk '
       function word_count(str,   count) {
         count = split(str, words, " ");
         return count
       }
       BEGIN { count = 0; }
       {
         if ($0 != "") {
           lines[count++] = $0;
         }
       }
       END {
         for (i = 0; i < count; i++) {
           printf("\n%s (%d)", lines[i], word_count(lines[i]));
         }
         printf("\n\nTotal non-empty lines: %d\n", count);
       }
     ' mary_had_a_little_lamb.txt
```

Verify any variables used in the user-defined function are actually locally
scoped. For example, in the model solution confirm the globally scoped variable
named `count` and the locally scoped variable named `count` do not interfere
with each other.
