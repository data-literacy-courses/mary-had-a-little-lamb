
The Mary Had a Little Lamb image was retrieved from Get Colorings - Color
Your Dreams (https://getcolorings.com) and used under the terms of the
Creative Commons Attribution-NonCommerical (CC-BY-NC) 4.0 International
License. To view a copy of this license, visit
https://creativecommons.org/licenses/by-nc/4.0/.
