\newpage

# Lesson 4 - Arrays, Loops and Repetition

By the end of this lesson you will:

- LO3 *Be able to alter the execution path of a programme with* decision
  statements and *various types of repetition*.
- LO4 *Use a basic data structure called an array to store a collection of
  related elements (items) instead of a separate variable for each*.

For each learning objective only the *italicised* portion is addressed in the
current lesson.

## Array

A data structure called an array provides a way to store a group of related
values using a singular variable and an index.

An array takes the form `variable_name[key] = value`, where *key* can be an
integer or a string.

In this lesson we store each line of the nursery rhyme in an array
demonstrating the three loop constructs.

## FOR Loop

The FOR loop is typically the preferred form of repeating a sequence of
statements when the number of iterations is known in advance or can be
computed. It is possible the statement block might never be run if the end
condition is true on the starting value of the iteration control variable.

The FOR statement takes the form,

```
for (variable = start_value; end_condition; variable_update) {
  statement_block
}
```

Let's see it in action.

```
awk '
  BEGIN {
    print("Counting up...")
    for (i = 1; i < 10; i++) {
      print("i =", i)
    }
  }
  END {
    print("Counting down...")
    for (i = 9; i > 0; i--) {
      print("i = ", i)
    }
  }
'
```

After reading the lines of the nursery rhyme and storing them in an array, a
FOR loop can be used to retrieve each line from the array in the same order as
they appear in the data file. The number of iterations must be known prior to
entering the statement block.

\newpage

```
awk '
  BEGIN { count = 0; }
  { lines[count++] = $0; }
  END {
    for (i = 0; i < count; i++) {
      print(lines[i]);
    }
  }
' mary_had_a_little_lamb.txt
```

## WHILE Loop

The WHILE loop is appropriate when repeating a sequence of statements and the
number of iterations is not known in advance. It is possible the statement
block might never be run if the end condition is true at the outset.

The WHILE statement takes the form,

```
variable = start_value;
WHILE (end_condition) {
  statement_block
}
```

After reading the lines of the nursery rhyme and storing them in an array, a
WHILE loop can be used to retrieve each line from the array in the same order
as they appear in the data file. The number of iterations depends entirely on
the value of the end condition.

```
awk '
   BEGIN { count = 0; }
  { lines[count++] = $0; }
  END {
     i = 0;
    while (i < count) {
       print(lines[i]);
      i++;
    }
  }
' mary_had_a_little_lamb.txt
```

## DO-WHILE Loop

The DO-WHILE loop is appropriate when repeating a sequence of statements and
the number of iterations is not known in advance.The statement block will be
run at least one time and loop until the end condition value is false.

The DO-WHILE statement takes the form,

```
do {
  statement_block
} while (end_condition)
```

After reading the lines of the nursery rhyme and storing them in an array, a
WHILE loop can be used to retrieve each line from the array in the same order
as they appear in the data file. The number of iterations does not need be
known prior to entering the statement block; it depends entirely on the value
of the end condition.

```
awk '
  BEGIN { count = 0; }
  { lines[count++] = $0; }
  END {
    i = 0;
    do {
      print(lines[i]);
      ++i;
    } while (i < count)
  }
' mary_had_a_little_lamb.txt
```

# Learner Challenge

Associated learning objective(s):

- LO2 *Understand* the purpose of a variable, *the ways in which variables are
  used*, and the differences between global and local scope.
- LO3 *Be able to alter the execution path of a programme with decision
  statements and various types of repetition*.
- LO4 *Use a basic data structure called an array to store a collection of
  related elements (items) instead of a separate variable for each*.

1. Modify the file reading programmes so that the statement block within the
loop does not execute at all, if possible.
2. Modify each version of the file reading programmes demonstrated in this
lesson by reversing the order in which the lines of the nursery rhyme are
displayed, that is, print the nursery rhyme beginning with the last line and
ending with the first line.
3. Modify any version of the file reading programme demonstrated in this lesson
such that only non-empty lines are stored in the array and display the array
contents.
